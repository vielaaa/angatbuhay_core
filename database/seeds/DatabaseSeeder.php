<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // for($x = 1; $x < 10 ; $x++){
            DB::table('users')->insert([
                'username' => str_random(10),
                'email' => str_random(10).'@gmail.com',
                'password' => hash('md5', 'secret'),
                'user_info_id' => '1'
            ]);            
        // }
    }
}
