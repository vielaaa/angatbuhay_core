<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cochampion extends Migration
{
    public function up()
    {
        Schema::create('co_champion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 50)->unique();
            $table->string('password', 70);
            $table->string('deputy_champion_email', 50)->index();
            $table->string('user_capabilities', 70);
            $table->foreign('deputy_champion_email')->references('email')->on('deputy_champion')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }
    public function down()
    {
        //
    }
}
