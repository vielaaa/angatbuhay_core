<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Champion extends Migration
{
    public function up()
    {
        Schema::create('champion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 50)->unique();
            $table->string('password', 70);
            $table->string('lgu_umbrella_name', 70);
            $table->text('geographical_coverage');
            $table->string('full_name', 70);
            $table->string('designation', 50);
            $table->string('organization', 40);
            $table->integer('tel_number')->nullable()->unsigned();
            $table->integer('mobile_number')->unsigned();
            $table->string('personal_narrative', 400);
            $table->text('photo_assets');
            $table->string('category', 20);
            $table->string('about_us', 400);
            $table->string('community_narrative', 400);
            $table->text('account_history');
            $table->timestamps();
        });
    }

    public function down()
    {
        //
    }
}
