<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class News extends Migration
{
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',70);
            $table->string('content', 50)->unique();
            $table->string('status', 10);
            $table->string('created_by', 50);
            $table->text('assets');
            $table->timestamps();
        });
    }
    public function down()
    {
        //
    }
}