<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Assets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('holder_email', 50)->nullable();
            $table->string('logo')->nullable();
            $table->string('banner')->nullable();
            $table->string('background')->nullable();
            $table->string('slider_image')->nullable();
            $table->string('cover_photo')->nullable();
            $table->string('display_photo')->nullable();
            $table->foreign('champion_email')->references('email')->on('champion')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
