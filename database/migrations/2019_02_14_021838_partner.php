<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Partner extends Migration
{
    public function up()
    {
        Schema::create('partner', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 50)->unique();
            $table->string('password', 70);
            $table->string('full_name', 70);
            $table->string('company_organization', 40);
            $table->string('category', 20);
            $table->string('company_description', 400);
            $table->string('designation', 50);
            $table->string('address_1', 70);
            $table->string('address_2', 70)->nullable();
            $table->string('address_city', 70);
            $table->string('address_province', 70);
            $table->string('address_country', 70);
            $table->integer('mobile_number')->unsigned();
            $table->integer('company_tel_number')->nullable()->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        //
    }
}
