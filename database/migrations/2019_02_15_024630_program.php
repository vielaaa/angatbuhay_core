<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Program extends Migration
{
    public function up()
    {
        Schema::create('program', function (Blueprint $table) {
            $table->increments('id');
            $table->string('champion_email', 50);
            $table->string('advocacy', 200);
            $table->string('geo_scope', 50);
            $table->string('keywords', 50);
            $table->string('program_title', 200);
            $table->string('program_pitch', 190);
            $table->string('program_challenge', 200);
            $table->string('program_vision', 300);
            $table->string('implementation_period', 200);
            $table->string('success_measure', 300);
            $table->string('plan_action', 300);
            $table->string('status', 50);
            $table->string('partner_id')->nullable();
            $table->text('program_assets', 400);
            $table->foreign('champion_email')->references('email')->on('champion')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }
    public function down()
    {
        //
    }
}
