<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Deputy extends Migration
{

    public function up()
    {
        Schema::create('deputy_champion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 50)->unique();
            $table->string('password', 70);
            $table->string('user_capabilities', 70);
            $table->string('champion_email', 50)->index();
            $table->foreign('champion_email')->references('email')->on('champion')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }
    public function down()
    {
        //
    }
}
