<?php

Route::group(['module' => 'Programs', 'middleware' => ['web'], 'namespace' => 'App\Modules\Programs\Controllers'], function() {

    Route::resource('Programs', 'ProgramsController');

});
