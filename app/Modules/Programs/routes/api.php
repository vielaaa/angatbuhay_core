<?php

Route::group(['module' => 'Programs', 'middleware' => ['api'], 'namespace' => 'App\Modules\Programs\Controllers', 'prefix' => 'program'], function() {

    Route::resource('Programs', 'ProgramsController');
    Route::get('/get-all-program', ['uses' => 'ProgramsController@getAllProgram','as'=>'getAllProgram']);
    Route::get('/get-specific-program', ['uses' => 'ProgramsController@getSpecificProgram','as'=>'getSpecificProgram']);
    Route::post('/delete-specific-program', ['uses' => 'ProgramsController@deleteSpecificProgram','as'=>'deleteSpecificProgram']);
    Route::post('/add-program', ['uses' => 'ProgramsController@addProgram','as'=>'addProgram']);
    Route::post('/update-program', ['uses' => 'ProgramsController@updateProgram','as'=>'updateProgram']);
    
});
