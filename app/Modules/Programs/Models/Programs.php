<?php

namespace App\Modules\Programs\Models;

use Illuminate\Database\Eloquent\Model;
use Hash;

class Programs extends Model {

    protected $table  = 'program';

    public static function getAllProgram(){
    	return self::all();
    }

    public static function getSpecificProgram($id){
        if ($program = self::where('id', $id)->first()) {
            return getAPIResponse('success', null, $program);
        }else{
            return getAPIResponse('fail', responseMessage('not-found','Program'));
        }
    }

    public static function deleteSpecificProgram($id){
        if (self::where('id', $id)->exists()) {
            self::find($id)->delete();
            return getAPIResponse('success', responseMessage('delete','Program'));
        }else{
            return getAPIResponse('fail', responseMessage('not-found','Program')); 
        }
    }

    public static function addProgram($data){
        if (self::where('champion_email', $data->champion_email)->exists()) {
            return getAPIResponse('fail', responseMessage('email-exist'));
        }else{
            $new_information = [
                'champion_email'    => $data->champion_email,
                'program_title'     => $data->program_title,
                'program_pitch'     => $data->program_pitch,
                'program_challenge' => $data->program_challenge,
                'program_vision'    => $data->program_vision,
                'advocacy'          => $data->advocacy,
                'geo_scope'         => $data->geo_scope,
                'keywords'          => $data->keywords,
                'implementation_period' => $data->implementation_period,
                'success_measure'   => $data->success_measure,
                'plan_action'       => $data->plan_action,
                'status'            => $data->status,
                'program_assets'    => assetsFormat($data->program_assets),
            ];
            self::insert($new_information);
            // $credentials = ['email' => $new_information['email'], 'password' => $new_information['password'], 'username' => $new_information['username']];
            // User::addUser($credentials);
            return getAPIResponse('success', responseMessage('create','Program'));
        }
    }

    public static function updateProgram($data){
        if ($program = self::where('id', $data->id)->first()) {
                $program->champion_email    = $data->champion_email;
                $program->program_title     = $data->program_title;
                $program->program_pitch     = $data->program_pitch;
                $program->program_challenge = $data->program_challenge;
                $program->program_vision    = $data->program_vision;
                $program->advocacy          = $data->advocacy;
                $program->geo_scope         = $data->geo_scope;
                $program->keywords          = $data->keywords;
                $program->implementation_period = $data->implementation_period;
                $program->success_measure   = $data->success_measure;
                $program->plan_action       = $data->plan_action;
                $program->status            = $data->status;
                $program->program_assets    = $data->program_assets;
                $program->update();

            return getAPIResponse('success', responseMessage('update','Program'));
        }else{
            return getAPIResponse('fail', responseMessage('not-found','Program'));;
        }
    }
}
