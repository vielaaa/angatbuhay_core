<?php

namespace App\Modules\Programs\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProgram;
use App\Http\Controllers\Controller;
use App\Modules\Programs\Models\Programs;
use Validator;

class ProgramsController extends Controller
{

    public function getAllProgram(){
        try {
           return getAPIResponse('success', requestMessage('success-req'), Programs::all());
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function getSpecificProgram(Request $request){
        try {
            return Programs::getSpecificProgram($request['id']);
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function deleteSpecificProgram(Request $request){
        try {
            return Programs::deleteSpecificProgram($request['id']);
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function addProgram(Request $request){
         $validator = Validator::make($request->all(), [
            'champion_email'    => 'required|email|max:20',
            'advocacy'          => 'required|max:200',
            'geo_scope'         => 'required|max:50',
            'keywords'          => 'required|max:50',
            'program_title'     => 'required|max:200',
            'program_pitch'     => 'required|max:190',
            'program_challenge' => 'required|max:200',
            'program_vision'    => 'required|max:300',
            'implementation_period' => 'required|max:200',
            'success_measure'   => 'required|max:300',
            'plan_action'       => 'required|max:300',
            'status'            => 'required|max:50',
            'program_assets'    => 'required'
        ]);

        if ($validator->fails()) {
            return getAPIResponse('fail',$validator->errors());
        }else{
            return Programs::addProgram($request);
        }
    }

    public function updateProgram(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'champion_email'    => 'required|email|max:20',
                'advocacy'          => 'required|max:200',
                'geo_scope'         => 'required|max:50',
                'keywords'          => 'required|max:50',
                'program_title'     => 'required|max:200',
                'program_pitch'     => 'required|max:190',
                'program_challenge' => 'required|max:200',
                'program_vision'    => 'required|max:300',
                'implementation_period' => 'required|max:200',
                'success_measure'   => 'required|max:300',
                'plan_action'       => 'required|max:300',
                'program_assets'    => 'required',
                'status'            => 'required|max:50',
            ]);

            if ($validator->fails()) {
                return getAPIResponse('fail',$validator->errors());
            }else{
                return Programs::updateProgram($request);
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }
}
