<?php

namespace App\Modules\News\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\News\Models\News;
use App\Modules\General\Models\User;
use Validator;

class NewsController extends Controller
{

    public function getAllNews()
    {
        try {
            return getAPIResponse('success', responseMessage('success-req'), News::all());
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
        
    }

    public function getSpecificNews(Request $request)
    {
        try {
            return News::getSpecificNews($request['id']);
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function deleteSpecificNews(Request $request)
    {
        try {
            return News::deleteSpecificNews($request['id']);
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function addNews(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title'=>'required|string|max:66',
                'content'=>'required|max:50',
                'assets'=>'required',
            ]);

            if ($validator->fails()) {
                return getAPIResponse('fail',$validator->errors());
            }else{
                return News::addNews($request);
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        } 
    }

    public function updateNews(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title'=>['required','string','max:66'],
                'content'=>'required|max:50',
                'assets'=>'required',
            ]);

            if ($validator->fails()) {
                return getAPIresponse('fail',$validator->errors());
            }else{
                return News::updateNews($request);
            }
            
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }
}
