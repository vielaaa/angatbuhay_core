<?php

namespace App\Modules\News\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;
class News extends Model {

    protected $table = 'news';
    // protected $guarded = ['email'];

    public static function getSpecificNews($id)
    {
        if(self::where('id',$id)->exists()){
            if ($info = self::where('id', $id)->first()) {
                return getApiResponse('success', responseMessage('success-req'), $info);
            }else{
                return getApiResponse('fail', responseMessage('not-found'));
            }
        }
        else{
            return getApiResponse('fail', responseMessage('not-found'));
        }
    }

    public static function deleteSpecificNews($id)
    {
        if (self::where('id', $id)->exists()) {
            self::find($id)->delete();
            return getApiResponse('success', responseMessage('delete'));
        }else{
            return getApiResponse('fail', responseMessage('not-found'));
        }
    }

    public static function addNews($data)
    {
        try {
            
            $news = new self;
            $news->title=$data->title;
            $news->content = $data->content;
            $news->assets = $data->assets;

            $news->save();
            return getApiResponse('success', responseMessage('create'));
            
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage()); 
        }
    }

    public static function updateNews($data)
    {
        try {
            if ($news = self::where('id', $data->id)->first()) {
                $news->title    =   $data->title;
                $news->content  =   $data->content;
                $news->assets   =   $data->assets;

                $news->update();
                return getApiResponse('success', responseMessage('update'));
            }else{
                return getApiResponse('fail', responseMessage('not-found'));
            }
        } catch (Exception $e) {
            return generalErorrResponse($e->getMessage());
        }
    }
}