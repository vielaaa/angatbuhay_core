<?php

Route::group(['module' => 'News', 'middleware' => ['web'], 'namespace' => 'App\Modules\News\Controllers'], function() {

    Route::resource('News', 'NewsController');

});
