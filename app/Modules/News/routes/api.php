<?php

Route::group(['module' => 'News', 'middleware' => ['api'], 'namespace' => 'App\Modules\News\Controllers', 'prefix' => 'news'], function() {

    Route::resource('News', 'NewsController');
    Route::get('/get-all-news', ['uses' => 'NewsController@getAllNews','as'=>'getAllNews']);
    Route::get('/get-specific-news', ['uses' => 'NewsController@getSpecificNews','as'=>'getSpecificNews']);
    Route::post('/add-news', ['uses' => 'NewsController@addNews','as'=>'addNews']);
    Route::post('/delete-specific-news', ['uses' => 'NewsController@deleteSpecificNews','as'=>'deleteSpecificNews']);
    Route::post('/update-news', ['uses' => 'NewsController@updateNews','as'=>'updateNews']);
});
