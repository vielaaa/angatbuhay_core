<?php

namespace App\Modules\General\Controllers;


use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\General\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class GeneralController extends Controller
{
    use AuthenticatesUsers;

    public function index()
    {
        return view("General::index");
    }

    public function userLogin(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $credentials = [
            'email' => $request["email"],
            'password' => $request["password"]
        ];

        if (Auth::guard('api')->attempt($credentials)) {
            return getApiResponse('success', 'logged in');
        }

        $this->incrementLoginAttempts($request);
        return getApiResponse('fail', 'invalid authentication');
    }
}
