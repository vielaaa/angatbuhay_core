<?php
/**
 *	General Helper  
 */

use App\Modules\General\Models\Admin;

function getAPIResponse($type, $message=null, $data=null){
	if ($type == 'success') {	
		$status = 0;
	}else if($type == 'fail'){
		$status = 1;
	}
	return ['status_code' => $status,'message' => $message, 'data' => $data];
}

function generalErrorResponse($errorMessage) {
    return '{"status_code": 999, "status": "fail", "message": "' . $errorMessage . '"}';
}
function responseMessage($type,$tag=null){
	if($tag==null){
		$tag='Record';
	}
	if($type=='create'){
		return $tag.' has been added.';
	}
	else if($type=='update'){
		return $tag.' updated.';
	}
	else if($type=='delete'){
		return $tag.' deleted.';
	}
	else if($type=='not-found'){
		return $tag.' not found.';
	}
	else if($type=='email-exist'){
		return 'Email has already been taken.';
	}
	else if($type=='success-req'){
		return 'Success request';
	}
}

function assetsFormat($data){
	$d[] = [{$data}];
	return $d;
}