<?php

namespace App\Modules\Champion\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\General\Models\User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

use Validator;

class CoChampion extends Authenticatable
{
	protected $table = 'co_champion';
	protected $guarded = 'email';

	public static function insertCoChampion($information, $champion_email){
		try {
			$data = json_decode($information);

			foreach ($data as $co_champion_meta) {
				$co_champion = new self;
				$co_champion->email = $co_champion_meta->email;
				$co_champion->password = bcrypt($co_champion_meta->password);
				$co_champion->deputy_champion_email = $champion_email;
				$co_champion->user_capabilities = $co_champion_meta->user_capabilities;
				$co_champion->save();
			}
			return getAPIResponse('success', responseMessage('create'));
		} catch (Exception $e) {
			return generalErrorResponse($e->getMessage());
		}
	}

	public static function updateCoChampion($data){
		try {
			if ($info = self::where('id', $data->id)->first()) {
				$info->email 		= $data['email'];
				$info->password 	= bcrypt($data['password']);
				$info->user_capabilities	= $data['user_capabilities'];
				$info->update();
				return getAPIResponse('success', responseMessage('update'));
			}
		} catch (Exception $e) {
			return generalErrorResponse($e->getMessage());	
		}
	}

	public static function deleteCoChampion($data){
		try {
			if (self::where('id', $data->id)->exists()) {
            	self::find($data->id)->delete();
            	return getAPIResponse('success', responseMessage('delete'));
        	}else{
            	return getAPIResponse('fail', responseMessage('not-found'));	
        	}
		} catch (Exception $e) {
			return generalErrorResponse($e->getMessage());			
		}
	}

	public static function getCoChampion($data){
		try {
			if ($co_champion = self::where('id', $data->id)->first()) {
				return getAPIResponse('success', null, $co_champion);
			}else{
				return getAPIResponse('fail', responseMessage('not-found'));
			}
		} catch (Exception $e) {
			return generalErrorResponse($e->getMessage());
		}
	}
}
