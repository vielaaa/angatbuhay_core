<?php

namespace App\Modules\Champion\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Champion\Models\Deputy;
use App\Modules\Champion\Models\CoChampion;
use App\Modules\General\Models\User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Hash;

class Champion extends Authenticatable {

	protected $table = 'champion';
	protected $guarded = 'email';

	public static function addChampion($data){
		if (self::where('email', $data->email)->exists()) {
            return getAPIResponse('fail', responseMessage('email-exist'));
		}else{
			$new_information = [
				'email' 			=> $data->email,
				'password' 			=> bcrypt($data->password),
				'lgu_umbrella_name' => $data->lgu_umbrella_name,
				'geographical_coverage'	=> $data->geographical_coverage,
				'full_name' 		=> $data->full_name,
				'designation' 		=> $data->designation,
				'organization' 		=> $data->organization,
				'tel_number' 		=> $data->tel_number,
				'mobile_number' 	=> $data->mobile_number,
				'personal_narrative' => $data->personal_narrative,
				'photo_assets' 		=> $data->photo_assets,
				'category' 			=> $data->category,
				'about_us' 			=> $data->about_us,
				'community_narrative' => $data->community_narrative,
				'account_history' 	=> $data->account_history,
			];
			self::insert($new_information);
			return getAPIResponse('success', responseMessage('create'));
		}
	}

	public static function deleteSpecificChampion($id){
    	if (self::where('id', $id)->exists()) {
            self::find($id)->delete();
            return getAPIResponse('success', responseMessage('delete'));
        }else{
            return getAPIResponse('fail', responseMessage('not-found'));	
        }
    }

    public static function updateChampion($data){
    	if ($champion = self::where('id', $data->id)->first()) {
			$champion->email 				= $data->email;
			$champion->password 			= bcrypt($data->password);
			$champion->lgu_umbrella_name	= $data->lgu_umbrella_name;
			$champion->geographical_coverage 	= $data->geographical_coverage;
			$champion->full_name			= $data->full_name;
			$champion->designation	 		= $data->designation;
			$champion->organization 		= $data->organization;
			$champion->tel_number	 		= $data->tel_number;
			$champion->mobile_number	 	= $data->mobile_number;
			$champion->personal_narrative	= $data->personal_narrative;
			$champion->photo_assets 		= $data->photo_assets;
			$champion->category 			= $data->category;
			$champion->about_us 			= $data->about_us;
			$champion->community_narrative	= $data->community_narrative;
			$champion->account_history	 	= $data->account_history;
    		$champion->update();

	    	return getAPIResponse('success', responseMessage('update'));
    	}else{
    		return getAPIResponse('fail', responseMessage('not-found'));
    	}
    }

    public static function getSpecificChampion($data){
    	if ($champion = self::where('id', $data->id)->first()) {
    		return getAPIResponse('success', null, championFormat($champion));
    	}else{
    		return getAPIResponse('fail', responseMessage('not-found'));
    	}
    }
}
