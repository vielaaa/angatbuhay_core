<?php

namespace App\Modules\Champion\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\General\Models\User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class Deputy extends Authenticatable
{
	protected $table = 'deputy_champion';
	protected $guarded = 'email';

	public static function insertDeputyChampion ($champion_email, $deputy_email, $deputy_password, $deputy_capabilities){
		try {
			if (self::where('email', $deputy_email)->exists()) {
				return getAPIResponse('fail', responseMessage('email-exist'));
			}else{
				$new_information = [
					'email' 			=> $deputy_email,
					'password' 			=> bcrypt($deputy_password),
					'user_capabilities' => $deputy_capabilities,
					'champion_email' 		=> $champion_email,
				];
				self::insert($new_information);
				// User::addUser(['email' => $deputy_email, 'password' => bcrypt($deputy_password), 'valid_email' => $champion_email]);
				return getAPIResponse('success', responseMessage('create'));
			}
		} catch (Exception $e) {
			return generalErrorResponse($e->getMessage());
		}
	}

	public static function updateDeputyChampion($data){
		try {
			if ($deputy = self::where('id', $data['id'])->first()) {
				$deputy->email 		= $data['email'];
				$deputy->password 	= bcrypt($data['password']);
				$deputy->user_capabilities	= $data['user_capabilities']; 
				$deputy->update();
				return getAPIResponse('success', responseMessage('update'));
			}
		} catch (Exception $e) {
			return generalErrorResponse($e->getMessage());	
		}
	}

	public static function deleteDeputyChampion($id){
		try {
			if (self::where('id', $id->id)->exists()) {
            	self::find($id->id)->delete();
            	return getAPIResponse('success', responseMessage('delete'));
        	}else{
            	return getAPIResponse('fail', responseMessage('not-found'));	
        	}
		} catch (Exception $e) {
			return generalErrorResponse($e->getMessage());			
		}
	}

	public static function getSpecificDeputy($data){
    	if ($champion = self::where('id', $data->id)->first()) {
    		return getAPIResponse('success', null, $champion);
    	}else{
    		return getAPIResponse('fail', responseMessage('not-found'));
    	}
    }
}
