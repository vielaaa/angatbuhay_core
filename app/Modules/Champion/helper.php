<?php
/**
 *	Champion Helper  
 */
use App\Modules\Champion\Models\Deputy;
use App\Modules\Champion\Models\CoChampion;

function coChampionFormat($co_champion){
	return json_encode([
		'email' 		=> $co_champion->email,
		'password'		=> bcrypt($co_champion->password)
	]);
}

function championFormat($champion){
	$deputy_champion = Deputy::where('champion_email', $champion['email'])->first();
	$co_champion = CoChampion::where('deputy_champion_email', $deputy_champion['email'])->get();
	return [
		'champion' 			=> $champion,
		'deputy_champion'	=> $deputy_champion,
		'co_champion'		=> json_decode($co_champion)
	];
}

function getChampionList($champion_list){
	$champ_list=array();
	foreach($champion_list as $champion)
	{
		$deputy_champion_data=Deputy::where('champion_email', $champion->email)->first();
		$co_champion_data= CoChampion::where('deputy_champion_email', $deputy_champion_data->email)->get();
		array_push($champ_list,['champion'=>$champion,'deputy_champion'=>$deputy_champion_data,'co_champion'=>json_decode($co_champion_data)]);
	}
	return $champ_list;
}