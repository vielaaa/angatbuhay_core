<?php

Route::group(['module' => 'Champion', 'middleware' => ['api'], 'namespace' => 'App\Modules\Champion\Controllers', 'prefix' => 'champion'], function() {

    Route::resource('Champion', 'ChampionController');

    Route::get('/get-all-champion', ['uses' => 'ChampionController@getAllChampion','as'=>'getAllChampion']);
    Route::get('/get-all-deputy', ['uses' => 'ChampionController@getAllDeputy','as'=>'getAllDeputy']);
    Route::get('/get-all-co-champion', ['uses' => 'ChampionController@getAllCoChampion','as'=>'getAllCoChampion']);
    Route::get('/get-specific-champion', ['uses' => 'ChampionController@getSpecificChampion','as'=>'getSpecificChampion']);
    Route::get('/get-specific-deputy', ['uses' => 'ChampionController@getSpecificDeputy','as'=>'getSpecificDeputy']);
    Route::get('/get-co-champion', ['uses' => 'ChampionController@getCoChampion','as'=>'getCoChampion']);
    Route::post('/add-champion', ['uses' => 'ChampionController@addChampion','as'=>'addChampion']);
    Route::post('/update-champion', ['uses' => 'ChampionController@updateChampion','as'=>'updateChampion']);
    Route::post('/delete-champion', ['uses' => 'ChampionController@deleteSpecificChampion','as'=>'deleteSpecificChampion']);

    Route::post('/update-deputy-champion', ['uses' => 'ChampionController@updateDeputyChampion','as'=>'updateDeputyChampion']);
    Route::post('/update-co-champion', ['uses' => 'ChampionController@updateCoChampion','as'=>'updateCoChampion']);
    Route::post('/delete-co-champion', ['uses' => 'ChampionController@deleteCoChampion','as'=>'deleteCoChampion']);
    Route::post('/delete-deputy-champion', ['uses' => 'ChampionController@deleteDeputyChampion','as'=>'deleteDeputyChampion']);

    Route::post('/login', ['uses' => 'ChampionController@loginChampionUsers','as' => 'login']);
});
