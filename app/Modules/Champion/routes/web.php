<?php

Route::group(['module' => 'Champion', 'middleware' => ['web'], 'namespace' => 'App\Modules\Champion\Controllers'], function() {

    Route::resource('Champion', 'ChampionController');

});
