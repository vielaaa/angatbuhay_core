<?php

namespace App\Modules\Champion\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Champion\Models\Champion;
use App\Modules\Champion\Models\CoChampion;
use App\Modules\Champion\Models\Deputy;
use Auth;
use Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class ChampionController extends Controller
{
    use AuthenticatesUsers;

    public function loginChampionUsers(Request $request){

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $credentials = [
            'email' => $request["email"],
            'password' => $request["password"]
        ];
        if ($request['login_type'] == 'champion') {
            $guard = 'Champion-api';
        }elseif($request['login_type'] == 'cochampion'){
            $guard = 'CoChampion-api';
        }elseif($request['login_type'] == 'deputy'){
            $guard = 'Deputy-api';
        }
        if (Auth::guard($guard)->attempt($credentials)) {
            return getApiResponse('success', 'logged in');
        }

        $this->incrementLoginAttempts($request);
        return getApiResponse('fail', 'invalid authentication');
    }

    public function getAllChampion(){
        try {
            return getAPIResponse('success', null, getChampionList(Champion::all()));
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());   
        }
    }

    public function getSpecificChampion(Request $id){
        return Champion::getSpecificChampion($id);
    }

    public function getCoChampion(Request $id){
        return CoChampion::getCoChampion($id);
    }

    public function getSpecificDeputy(Request $id){
        return Deputy::getSpecificDeputy($id);
    }

    public function addChampion(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'email'             => 'required|email|max:50|unique:champion',
                'password'          => 'required|max:10',
                'lgu_umbrella_name' => 'required|max:70',
                'geographical_coverage' => 'required',
                'full_name'         => 'required|max:50',
                'designation'       => 'required|max:50',
                'organization'      => 'required|max:40',
                'tel_number'        => 'max:11|numeric',
                'mobile_number'     => 'required|max:11|numeric',
                'personal_narrative' => 'required|max:400',
                'photo_assets'      => 'required',
                'category'          => 'required|max:20',
                'about_us'          => 'required|max:400',
                'community_narrative'       => 'required|max:400',
                'deputy_champion_email'     => 'max:50',
                'deputy_capabilities'       => 'max:70',
                'deputy_champion_password'  => 'max:10',
            ]);
            
            if ($validator->fails()) {
                return getAPIResponse('fail', $validator->errors());
            }else{
                if ($data = Champion::addChampion($request)['status'] == 0) {
                    if ($data = Deputy::insertDeputyChampion($request['email'], $request['deputy_champion_email'], $request['deputy_champion_password'], $request['deputy_capabilities'])['status'] == 0) {
                        if ($request['co_champion_details'] != null) {
                            $data = CoChampion::insertCoChampion($request['co_champion_details'], $request['deputy_champion_email']);
                        }
                    }      
                }
                return $data;
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function updateChampion(Request $request){
        // $validate = validateChampionRequest($request);
        try {
            $validator = Validator::make($request->all(), [
                'email'             => 'required|email|max:50|unique:champion,email,'.$request->id,
                'password'          => 'required|max:10',
                'lgu_umbrella_name' => 'required|max:70',
                'geographical_coverage' => 'required',
                'full_name'         => 'required|max:50',
                'designation'       => 'required|max:50',
                'organization'      => 'required|max:40',
                'tel_number'        => 'numeric',
                'mobile_number'     => 'required|numeric',
                'personal_narrative' => 'required|max:400',
                'photo_assets'      => 'required',
                'category'          => 'required|max:20',
                'about_us'          => 'required|max:400',
                'community_narrative' => 'required|max:400',
            ]);

            if ($validator->fails()) {
                return getAPIResponse('fail',$validator->errors());
            }else{
                return Champion::updateChampion($request);
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function updateDeputyChampion(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'email'=>'required|email|max:50|unique:deputy_champion,email,'.$request->id,
                'password'=>'required|max:70',
                'user_capabilities'=>'required|max:70',
            ]);

            if ($validator->fails()) {
                return getAPIResponse('fail',$validator->errors());
            }else{
                return Deputy::updateDeputyChampion($request);
            }
            
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function updateCoChampion(Request $request){
        try {

            $validator = Validator::make($request->all(), [
                'email'=>'required|email|max:50|unique:co_champion,email,'.$request->id,
                'password'=>'required|max:70',
                'user_capabilities'=>'required|max:70',
            ]);

            if ($validator->fails()) {
                return getAPIResponse('fail',$validator->errors());
            }else{
                return CoChampion::updateCoChampion($request);
            }
            
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());   
        }
    }

    public function deleteSpecificChampion(Request $request){
        try {
            return Champion::deleteSpecificChampion($request['id']);
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function deleteCoChampion(Request $request){
        try {
            return CoChampion::deleteCoChampion($request);
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function deleteDeputyChampion(Request $request){
        try {
            return Deputy::deleteDeputyChampion($request);
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function getAllCoChampion(){
        try {
            return getAPIResponse('success', null, CoChampion::all());
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }
    public function getAllDeputy(){
        try {
            return getAPIResponse('success', null, Deputy::all());
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }
}
