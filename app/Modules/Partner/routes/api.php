<?php

Route::group(['module' => 'Partner', 'middleware' => ['api'], 'namespace' => 'App\Modules\Partner\Controllers', 'prefix' => 'partner'], function() {

    Route::resource('Partner', 'PartnerController');

    Route::get('/get-all-partner', ['uses' => 'PartnerController@getAllPartner','as'=>'getAllPartner']);
    Route::get('/get-specific-partner', ['uses' => 'PartnerController@getSpecificPartner','as'=>'getSpecificPartner']);
    Route::post('/add-partner', ['uses' => 'PartnerController@addPartner','as'=>'addPartner']);
    Route::post('/delete-specific-partner', ['uses' => 'PartnerController@deleteSpecificPartner','as'=>'deleteSpecificPartner']);
    Route::post('/update-partner', ['uses' => 'PartnerController@updatePartner','as'=>'updatePartner']);

});
