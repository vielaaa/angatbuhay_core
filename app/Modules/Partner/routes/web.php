<?php

Route::group(['module' => 'Partner', 'middleware' => ['web'], 'namespace' => 'App\Modules\Partner\Controllers'], function() {

    Route::resource('Partner', 'PartnerController');

});
