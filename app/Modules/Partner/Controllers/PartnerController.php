<?php

namespace App\Modules\Partner\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Partner\Models\Partner;
use Validator;

class PartnerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Partner::index");
    }

    public function getAllPartner()
    {
        try{
            return getAPIResponse('success', responseMessage('success-req'), Partner::all());
        }
        catch(Exception $e){
            return generalErrorResponse($e->getMessage());
        }
    }

    public function getSpecificPartner(Request $request)
    {
        try{
            return Partner::getSpecificPartner($request);
        }
        catch(Exeption $e)
        {
            return generalErrorResponse($e->getMessage());
        }
         
    }

    public function addPartner(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email'                 => 'required|email|max:50|unique:partner',
                'password'              => 'required|max:70',
                'full_name'             => 'required|max:70',
                'company_organization'  => 'required|max:40',
                'category'              => 'required|max:20',
                'company_description'   => 'required|max:400',
                'designation'           => 'required|max:50',
                'company_address_1'     => 'required|max:70',
                'company_address_2'     => 'max:70',
                'address'               => 'required|max:70',
                'province'              => 'max:70',
                'country'               => 'required|max:70',
                'mobile_number'         => 'required|max:10',
                'tel_number'            => 'required|max:10', 
            ]);

            if ($validator->fails()) {
                return getAPIResponse('fail', $validator->errors());
            }else{
                return Partner::addPartner($request);
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function updatePartner(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email'                 => 'required|email|max:50|unique:partner,email,'.$request->id,
                'password'              => 'required|max:70',
                'full_name'             => 'required|max:70',
                'company_organization'  => 'required|max:40',
                'category'              => 'required|max:20',
                'company_description'   => 'required|max:400',
                'designation'           => 'required|max:50',
                'company_address_1'     => 'required|max:70',
                'company_address_2'     => 'max:70',
                'address'               => 'required|max:70',
                'province'              => 'max:70',
                'country'               => 'required|max:70',
                'mobile_number'         => 'required|max:10',
                'tel_number'            => 'required|max:10', 
            ]);

            if ($validator->fails()) {
                return getAPIResponse('fail',$validator->errors());
            }else{
                return Partner::updatePartner($request);
            }
            
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        } 
    }

    public function deleteSpecificPartner(Request $request){
        try{
            return Partner::deleteSpecificPartner($request->id);
        }catch(Exception $e)
        {
            return getErrorResponse($e->getMessage());
        }
    }
}
