<?php

namespace App\Modules\Partner\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\General\Models\User;

class Partner extends Model {

    //
    protected $table = 'partner';
    protected $guarded = 'email';

    public static function addPartner($data)
    {
    	try {
            if (self::where('email', $data->email)->exists()) {
                return getAPIResponse('fail', responseMessage('email-exist'));
            }else{
                $partner = new self;
                $partner->email                 =   $data->email;
				$partner->password              =   bcrypt($data->password);
                $partner->full_name             =   $data->full_name;
                $partner->company_organization  =   $data->company_organization;
                $partner->category              =   $data->category;
                $partner->company_description   =   $data->company_description;
                $partner->designation           =   $data->designation;
                $partner->address_1             =   $data->company_address_1;
                $partner->address_2             =   $data->company_address_2;
                $partner->address_city          =   $data->address;
                $partner->address_province      =   $data->province;
                $partner->address_country       =   $data->country;
                $partner->mobile_number         =   $data->mobile_number;
                $partner->company_tel_number    =   $data->tel_number;


                $partner->save();
                return getAPIResponse('success', responseMessage('create'));
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public static function getSpecificPartner($data)
    {
        try{
            if ($info = self::where('id', $data->id)->first()) {
                return getApiResponse('success', responseMessage('success-req'), $info);
            }else{
                return getApiResponse('fail', responseMessage('not-found'));
            }
        }catch(Exception $e)
        {
            return generalErrorResponse($e->getMessage());
        }
    }

    public static function updatePartner($data)
    {
        try {
            if ($partner = self::where('id', $data->id)->first()) {
                $partner->email                 =   $data->email;
				$partner->password              =   bcrypt($data->password);
                $partner->full_name             =   $data->full_name;
                $partner->company_organization  =   $data->company_organization;
                $partner->category              =   $data->category;
                $partner->company_description   =   $data->company_description;
                $partner->designation           =   $data->designation;
                $partner->address_1             =   $data->company_address_1;
                $partner->address_2             =   $data->company_address_2;
                $partner->address_city          =   $data->address;
                $partner->address_province      =   $data->province;
                $partner->address_country       =   $data->country;
                $partner->mobile_number         =   $data->mobile_number;
                $partner->company_tel_number    =   $data->tel_number;

                $partner->update();
                return getAPIResponse('success', responseMessage('update'));
            }else{
                return getAPIResponse('fail', responseMessage('not-found'));
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage()); 
        }
    }

    public static function deleteSpecificPartner($id)
    {
        try{
            if (self::where('id', $id)->exists()) {
                self::find($id)->delete();
                return getApiResponse('success', responseMessage('delete'));
            }else{
                return getApiResponse('fail', responseMessage('not-found'));
            }
        }catch(Exception $e){
            return generalErrorResponse($e->getMessage());
        }
    }
}
