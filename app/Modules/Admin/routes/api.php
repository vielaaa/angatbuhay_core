<?php

Route::group(['module' => 'Admin', 'middleware' => ['api'], 'namespace' => 'App\Modules\Admin\Controllers', 'prefix' => 'admin'], function() {

    Route::resource('Admin', 'AdminController');

    Route::post('/login', ['uses' => 'AdminController@login','as'=>'login']);

    Route::post('/add-admin', ['uses' => 'AdminController@addAdmin','as'=>'addAdmin']);
    Route::post('/update-admin', ['uses' => 'AdminController@updateAdmin','as'=>'updateAdmin']);
    Route::post('/delete-admin', ['uses' => 'AdminController@deleteAdmin','as'=>'deleteAdmin']);
    Route::get('/get-all-admin', ['uses' => 'AdminController@getAllAdmin','as'=>'getAllAdmin']);
    Route::get('/get-specific-admin', ['uses' => 'AdminController@getSpecificAdmin','as'=>'getSpecificAdmin']);
});
