<?php

namespace App\Modules\Admin\Controllers;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Modules\Admin\Models\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request){
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        $credentials = [
            'email' => $request["email"],
            'password' => $request["password"]
        ];
        if (Auth::guard('api')->attempt($credentials)) {
            return getApiResponse('success', 'logged in');
        }
        $this->incrementLoginAttempts($request);
        return getApiResponse('fail', 'invalid authentication');
    }
    public function getAllAdmin()
    {
        try {
            return getAPIResponse('success', null, Admin::all());
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function getSpecificAdmin(Request $request)
    {
        try {
            if ($data = Admin::where('id', $request['id'])->exists()) {
                return getAPIResponse('success', null, $data);
            }else{
                return getAPIResponse('fail', responseMessage('not-found','User'));
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function addAdmin(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email'             => 'required|email|max:50',
                'password'          => 'required|max:10',
                'full_name'         => 'required|max:50',
            ]);  
            if ($validator->fails()) {
                return getAPIResponse('fail', $validator->errors());
            }else{
                return Admin::addAdmin($request);
            }
            
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function updateAdmin(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email'             => 'required|email|max:50',
                'password'          => 'required|max:10',
                'full_name'         => 'required|max:50',
            ]);

            
            if ($validator->fails()) {
                return getAPIResponse('fail', $validator->errors());
            }else{
                return Admin::updateAdmin($request);
            }
            
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }

    public function deleteAdmin(Request $request)
    {
        try {
            if ($data = Admin::where('id', $request['id'])->exists()) {
                Admin::find($request['id'])->delete();
                return getAPIResponse('success', responseMessage('delete','User'));
            }else{
                return getAPIResponse('fail', responseMessage('not-found','User'));
            }
        } catch (Exception $e) {
            return generalErrorResponse($e->getMessage());
        }
    }
}
