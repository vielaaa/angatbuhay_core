<?php

namespace App\Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class Admin extends Authenticatable {

    protected $table = 'admin';
    protected $guarded = 'email';

    public static function addAdmin($data)
    {
    	try {
    		if (self::where('email', $data->email)->exists()) {
    			return getAPIResponse('fail', responseMessage('email-exist'));
    		}else{
    			$admin = new self;
    			$admin->email 		= $data->email;
    			$admin->password 	= bcrypt($data->password);
    			$admin->full_name	= $data->full_name;
    			$admin->save();

    			return getAPIResponse('success', responseMessage('create'));
    		}
    	} catch (Exception $e) {
            return generalErrorResponse($e->getMessage());    		
    	}
    }

    public static function updateAdmin($data)
    {
    	try {
    		if ($admin = self::where('id', $data->id)->first()) {
    			if ($admin->email != $data->email && self::where('email', $data->email)->exists()) {
					return getAPIResponse('success', responseMessage('email-exist'));
    			}else{
    				$admin->email = $data->email;
    				$admin->password = bcrypt($data->password);
    				$admin->full_name = $data->full_name;
    				$admin->update();
    				return getAPIResponse('success', responseMessage('update'));
    			}
    		}else{
    			return getAPIResponse('fail', responseMessage('not-found'));
    		}
    	} catch (Exception $e) {
            return generalErrorResponse($e->getMessage());    		
    	}
    }
}
