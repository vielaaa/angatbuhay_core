<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProgram extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'             => 'required|email|max:20',
            'program_title'     => 'required|max:200',
            'program_pitch'     => 'required|max:190',
            'program_challenge' => 'required|max:200',
            'program_vision'    => 'required|max:300',
            'implementation_period' => 'required|max:200',
            'success_measure'   => 'required|max:300',
            'action_plan'       => 'required|max:300',
            'plan_action'       => 'required|max:300',
            'program_assets'    => 'required',
            'address'           => 'required'
        ];
    }
}
